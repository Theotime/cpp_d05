#ifndef __CENTRALBUREAUCRACY_HPP__
# define __CENTRALBUREAUCRACY_HPP__

# include <iostream>
# include "OfficeBlock.hpp"
# include "Bureaucrat.hpp"
# include "Intern.hpp"

class CentralBureaucracy {

	public:
		CentralBureaucracy();
		~CentralBureaucracy();

		void					feed(Bureaucrat *obj);
		void					queueUp(std::string const &target);
		void					doBureaucracy(void);

		int						getNbTargets(void) const;
	private:
		CentralBureaucracy(CentralBureaucracy const &obj);
		CentralBureaucracy		&operator=(CentralBureaucracy const &obj);

		std::string				getNextQueue(void);

		std::string				*_targets;
		OfficeBlock				_offices[20];
		int						_nbTargets;


};

#endif
