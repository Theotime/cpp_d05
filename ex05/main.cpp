#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "Intern.hpp"
#include "OfficeBlock.hpp"
#include "CentralBureaucracy.hpp"
#include <sstream>

int main()
{
	CentralBureaucracy		central;
	Bureaucrat				*toto;
	std::stringstream		name;

	std::srand((time_t) &central);
	for (size_t i = 0 ; i < 20; ++i) {
		name << "Toto " << i;
		toto = new Bureaucrat(name.str(), (std::rand() % 149) + 1);
		std::cout << "[ CREATE Bureaucrat ] -> Name : " << toto->getName() << " | Grade : " << toto->getGrade() << std::endl;
		name.str("");
		central.feed(toto);
	}

	for (size_t i = 0; i < 200 ; ++i) {
		name << "Client " << i;
		central.queueUp(name.str());
		name.str("");
	}
	std::cout << "NB TARGET : " << central.getNbTargets() << std::endl;
	central.doBureaucracy();
	std::cout << "NB TARGET : " << central.getNbTargets() << std::endl;

}
