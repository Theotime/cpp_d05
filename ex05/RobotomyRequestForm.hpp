#ifndef __ROBOTOMYREQUESTFORM_HPP__
# define __ROBOTOMYREQUESTFORM_HPP__

# include "Form.hpp"

class RobotomyRequestForm : public Form {

	public:
		RobotomyRequestForm(std::string const &target);
		RobotomyRequestForm(RobotomyRequestForm const &obj);
		~RobotomyRequestForm();

		RobotomyRequestForm			&operator=(RobotomyRequestForm const &obj);

		virtual void						execute(Bureaucrat const &obj) const throw(Form::NotSignedException, Form::GradeTooLowException);

	protected:
		RobotomyRequestForm();
};

#endif