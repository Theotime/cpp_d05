#include "CentralBureaucracy.hpp"

CentralBureaucracy::CentralBureaucracy() : _targets(0), _nbTargets(0) {
	Intern				*tmp;

	for (size_t i = 0; i < 20; ++i) {
		tmp = new Intern();
		this->_offices[i].setIntern(*tmp);
	}
}

CentralBureaucracy::CentralBureaucracy(CentralBureaucracy const &obj) {
	*this = obj;
}

CentralBureaucracy::~CentralBureaucracy() {}

CentralBureaucracy				&CentralBureaucracy::operator=(CentralBureaucracy const &obj) {
	(void)obj;
	return (*this);
}

void							CentralBureaucracy::feed(Bureaucrat *obj) {
	for (size_t i = 0; i < 20; ++i) {
		if (!this->_offices[i].hasSigner()) {
			this->_offices[i].setSigner(*obj);
			return ;
		} else if (!this->_offices[i].hasExecutor()) {
			this->_offices[i].setExecutor(*obj);
			return ;
		}
	}
}

void							CentralBureaucracy::queueUp(std::string const &target) {
	int				i = 0;

	std::string		*targets = new std::string[this->_nbTargets + 1];
	std::cout << target << " " << this->_nbTargets << std::endl;
	for (; i < this->_nbTargets; ++i) {
		targets[i] = this->_targets[i];
	}
	targets[i] = target;
	this->_targets = targets;
	++this->_nbTargets;
}

void							CentralBureaucracy::doBureaucracy(void) {
	std::string			next;
	size_t				n = 0;

	next = this->getNextQueue();
	while (this->_nbTargets > 0) {
		this->_offices[n++].doBureaucracy("presidential pardon", next);
		n = n < 20 ? n : 0;
		next = this->getNextQueue();
	}
}

std::string						CentralBureaucracy::getNextQueue(void) {
	std::string				*targets = new std::string[this->_nbTargets - 1];
	std::string				next;

	if (this->_nbTargets <= 0)
		return (0);
	next = this->_targets[0];
	for (int i = 1; i < this->_nbTargets; ++i) {
		targets[i] = this->_targets[i];
	}
	this->_targets = targets;
	--this->_nbTargets;
	return (next);
}

int								CentralBureaucracy::getNbTargets(void) const {
	return (this->_nbTargets);
}
