#include "OfficeBlock.hpp"
#include "Form.hpp"

OfficeBlock::OfficeBlock() : _intern(0), _signer(0), _executor(0) {}

OfficeBlock::OfficeBlock(Intern *intern, Bureaucrat *signer, Bureaucrat *executor) :
	_intern(intern),
	_signer(signer),
	_executor(executor)
{}

OfficeBlock::OfficeBlock(OfficeBlock const &obj) {
	*this = obj;
}

OfficeBlock::~OfficeBlock() {}

OfficeBlock				&OfficeBlock::operator=(OfficeBlock const &obj) {
	this->_intern = obj._intern;
	this->_signer = obj._signer;
	this->_executor = obj._executor;
	return (*this);
}

void					OfficeBlock::doBureaucracy(std::string const &type, std::string const &target) {
	Form			*f;

	if (!this->isReady())
		return ;
	f = this->_intern->makeForm(type, target);
	if (!f)
		return ;
	this->_signer->signForm(*f);
	this->_executor->executeForm(*f);
}

bool					OfficeBlock::isReady(void) const {
	return (this->_intern != 0 && this->_signer != 0 && this->_executor != 0);
}

void					OfficeBlock::setIntern(Intern &obj) {
	this->_intern = &obj;
}

void					OfficeBlock::setSigner(Bureaucrat &obj) {
	this->_signer = &obj;
}

void					OfficeBlock::setExecutor(Bureaucrat &obj) {
	this->_executor = &obj;
}

bool					OfficeBlock::hasIntern(void) const {
	return (!!this->_intern);
}

bool					OfficeBlock::hasSigner(void) const {
	return (!!this->_signer);
}

bool					OfficeBlock::hasExecutor(void) const {
	return (!!this->_executor);
}
