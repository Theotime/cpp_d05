#ifndef __FORM_HPP__
# define __FORM_HPP__

# include <iostream>
# include <stdexcept>
# include "Bureaucrat.hpp"

class Form {

	public:
		Form(Form const &obj);
		Form(std::string const &name, int gradeForSigned, int gradeForExecute);
		~Form();

		void					beSigned(Bureaucrat const &obj);

		std::string const 		getName(void) const;
		int						getGradeForSigned(void) const;
		int						getGradeForExecute(void) const;
		bool					getSigned(void) const;

		void					setGradeForSigned(int n);
		void					setGradeForExecute(int n);

		Form					&operator=(Form const &obj);

		class GradeTooHighException : public std::exception {
			public:
				virtual const char*			what() const throw() {
					return ("EXCEPTION -> Form::GradeTooHighException");
				}
		};

		class GradeTooLowException : public std::exception {
			public:
				virtual const char*			what() const throw() {
					return ("EXCEPTION -> Form::GradeTooLowException");
				}
		};


	private:
		Form();
		std::string			_name;
		int					_gradeForSigned;
		int					_gradeForExecute;
		bool				_signed;

};

std::ostream				&operator<<(std::ostream &stream, Form const &obj);

#endif