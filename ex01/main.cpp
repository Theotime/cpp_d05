#include "Bureaucrat.hpp"
#include "Form.hpp"

int				main() {
	{
		Bureaucrat		sarkozy("Sarkozy", 5);
		Form			l1("Lois 1", 15, 42);
		Form			l2("Lois 2", 110, 3);
		Form			l3("Lois 3", 1, 1);

		sarkozy.signForm(l1);
		sarkozy.signForm(l2);
		sarkozy.signForm(l3);
	}
	{
		Bureaucrat		hollande("Hollande", 100);
		Form			l1("Lois 1", 15, 42);
		Form			l2("Lois 2", 110, 3);
		Form			l3("Lois 3", 1, 1);

		hollande.signForm(l1);
		hollande.signForm(l2);
		hollande.signForm(l3);
	}

}