#include "Form.hpp"

Form::Form() {}

Form::Form(Form const &obj) {
	*this = obj;
}

Form::Form(std::string const &name, int gradeForSigned, int gradeForExecute) :
	_name(name)
{
	this->setGradeForSigned(gradeForSigned);
	this->setGradeForExecute(gradeForExecute);
}

Form::~Form() {}

std::ostream				&operator<<(std::ostream &stream, Form const &obj) {
	stream << "Form : " << obj.getName();
	stream << " | for signed : " << obj.getGradeForSigned();
	stream << " | for execute : " << obj.getGradeForExecute();
	stream << " | is signed : " << obj.getSigned();
	return (stream);
}

Form			&Form::operator=(Form const &obj) {
	this->_gradeForSigned = obj._gradeForSigned;
	this->_gradeForExecute = obj._gradeForExecute;
	this->_name = obj._name;
	this->_signed = obj._signed;
	return (*this);
}

void							Form::beSigned(Bureaucrat const &obj) {
	if (this->getGradeForSigned() < obj.getGrade())
		throw Form::GradeTooLowException();
	this->_signed = true;
}

std::string const 				Form::getName(void) const {
	return (this->_name);
}

int								Form::getGradeForSigned(void) const {
	return (this->_gradeForSigned);
}

int								Form::getGradeForExecute(void) const {
	return (this->_gradeForExecute);
}

bool							Form::getSigned(void) const {
	return (this->_signed);
}

void							Form::setGradeForSigned(int n) {
	if (n > 150)
		throw Form::GradeTooLowException();
	else if (n < 1)
		throw Form::GradeTooHighException();
	this->_gradeForSigned = n;
}

void							Form::setGradeForExecute(int n) {
	if (n > 150)
		throw Form::GradeTooLowException();
	else if (n < 1)
		throw Form::GradeTooHighException();
	this->_gradeForExecute = n;
}