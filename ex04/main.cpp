#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "Intern.hpp"
#include "OfficeBlock.hpp"

int main()
{
	{
		Intern idiotOne;
		Bureaucrat hermes = Bureaucrat("Hermes Conrad", 37);
		Bureaucrat bob = Bureaucrat("Bobby Bobson", 123);
		OfficeBlock ob;
		ob.setIntern(idiotOne);
		ob.setSigner(bob);
		ob.setExecutor(hermes);

		try {
			ob.doBureaucracy("FORM NOT FOUND", "Pigley");
		} catch (Form::GradeTooHighException &e) {
			std::cout << "FORM GradeTooHighException : " << e.what() << std::endl;
		}  catch (Form::GradeTooLowException &e) {
			std::cout << "FORM GradeTooLowException : " << e.what() << std::endl;
		} catch (Form::NotSignedException &e) {
			std::cout << "FORM NotSignedException : " << e.what() << std::endl;
		} catch (std::exception & e) {
			std::cout << "STD EXCEPTION : " << e.what() << std::endl;
		}
		std::cout << std::endl;
	}
	{
		Intern idiotOne;
		Bureaucrat hermes = Bureaucrat("Hermes Conrad", 37);
		Bureaucrat bob = Bureaucrat("Bobby Bobson", 123);
		OfficeBlock ob;
		ob.setIntern(idiotOne);
		ob.setSigner(bob);
		ob.setExecutor(hermes);

		try {
			ob.doBureaucracy("presidential pardon", "Pigley");
		} catch (Form::GradeTooHighException &e) {
			std::cout << "FORM GradeTooHighException : " << e.what() << std::endl;
		}  catch (Form::GradeTooLowException &e) {
			std::cout << "FORM GradeTooLowException : " << e.what() << std::endl;
		} catch (Form::NotSignedException &e) {
			std::cout << "FORM NotSignedException : " << e.what() << std::endl;
		} catch (std::exception & e) {
			std::cout << "STD EXCEPTION : " << e.what() << std::endl;
		}
		std::cout << std::endl;
	}
	{
		Intern idiotOne;
		Bureaucrat hermes = Bureaucrat("Hermes Conrad", 150);
		Bureaucrat bob = Bureaucrat("Bobby Bobson", 123);
		OfficeBlock ob;
		ob.setIntern(idiotOne);
		ob.setSigner(bob);
		ob.setExecutor(hermes);

		try {
			ob.doBureaucracy("presidential pardon", "Pigley");
		} catch (Form::GradeTooHighException &e) {
			std::cout << "FORM GradeTooHighException : " << e.what() << std::endl;
		}  catch (Form::GradeTooLowException &e) {
			std::cout << "FORM GradeTooLowException : " << e.what() << std::endl;
		} catch (Form::NotSignedException &e) {
			std::cout << "FORM NotSignedException : " << e.what() << std::endl;
		} catch (std::exception & e) {
			std::cout << "STD EXCEPTION : " << e.what() << std::endl;
		}
		std::cout << std::endl;
	}
	{
		Intern idiotOne;
		Bureaucrat hermes = Bureaucrat("Hermes Conrad", 37);
		Bureaucrat bob = Bureaucrat("Bobby Bobson", 150);
		OfficeBlock ob;
		ob.setIntern(idiotOne);
		ob.setSigner(bob);
		ob.setExecutor(hermes);

		try {
			ob.doBureaucracy("presidential pardon", "Pigley");
		} catch (Form::GradeTooHighException &e) {
			std::cout << "FORM GradeTooHighException : " << e.what() << std::endl;
		}  catch (Form::GradeTooLowException &e) {
			std::cout << "FORM GradeTooLowException : " << e.what() << std::endl;
		} catch (Form::NotSignedException &e) {
			std::cout << "FORM NotSignedException : " << e.what() << std::endl;
		} catch (std::exception & e) {
			std::cout << "STD EXCEPTION : " << e.what() << std::endl;
		}
		std::cout << std::endl;
	}
	{
		Intern idiotOne;
		Bureaucrat hermes = Bureaucrat("Hermes Conrad", 150);
		Bureaucrat bob = Bureaucrat("Bobby Bobson", 150);
		OfficeBlock ob;
		ob.setIntern(idiotOne);
		ob.setSigner(bob);
		ob.setExecutor(hermes);

		try {
			ob.doBureaucracy("presidential pardon", "Pigley");
		} catch (Form::GradeTooHighException &e) {
			std::cout << "FORM GradeTooHighException : " << e.what() << std::endl;
		}  catch (Form::GradeTooLowException &e) {
			std::cout << "FORM GradeTooLowException : " << e.what() << std::endl;
		} catch (Form::NotSignedException &e) {
			std::cout << "FORM NotSignedException : " << e.what() << std::endl;
		} catch (std::exception & e) {
			std::cout << "STD EXCEPTION : " << e.what() << std::endl;
		}
	}
}