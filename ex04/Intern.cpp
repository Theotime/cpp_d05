#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"


Intern::Intern() {}
Intern::Intern(Intern const &obj) {
	*this = obj;
}
Intern::~Intern() {}

Intern				&Intern::operator=(Intern const &obj) {
	(void)obj;
	return (*this);
}

Form				*Intern::makeForm(std::string const &type, std::string const &target) const {
	Form			*form = 0;

	if (!type.compare("presidential pardon"))
		form = new PresidentialPardonForm(target);
	else if (!type.compare("robotomy request"))
		form = new RobotomyRequestForm(target);
	else if (!type.compare("shrubbery creation"))
		form = new ShrubberyCreationForm(target);
	else {
		std::cout << "Intern cannot be create form " << type << " for " << target << std::endl;
		return (0);
	}
	std::cout << "Intern creates " << form->getName() << std::endl;
	return (form);
}