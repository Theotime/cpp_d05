#ifndef __OFFICEBLOCK_HPP__
# define __OFFICEBLOCK_HPP__

# include "Bureaucrat.hpp"
# include "Intern.hpp"

class OfficeBlock {

	public:
		OfficeBlock();
		OfficeBlock(Intern *intern, Bureaucrat *signer, Bureaucrat *executor);
		~OfficeBlock();

		void				doBureaucracy(std::string const &type, std::string const &target);

		void				setIntern(Intern &obj);
		void				setSigner(Bureaucrat &obj);
		void				setExecutor(Bureaucrat &obj);


	private:
		OfficeBlock(OfficeBlock const &obj);
		OfficeBlock				&operator=(OfficeBlock const &obj);
		bool					isReady(void) const;

		Intern				*_intern;
		Bureaucrat			*_signer;
		Bureaucrat			*_executor;
		

};

#endif