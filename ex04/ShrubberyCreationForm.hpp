#ifndef __SHRUBBERYCREATIONFORM_HPP__
# define __SHRUBBERYCREATIONFORM_HPP__

# include "Form.hpp"

class ShrubberyCreationForm : public Form {

	public:
		ShrubberyCreationForm(std::string const &target);
		ShrubberyCreationForm(ShrubberyCreationForm const &obj);
		~ShrubberyCreationForm();

		ShrubberyCreationForm				&operator=(ShrubberyCreationForm const &obj);

		virtual void						execute(Bureaucrat const &obj) const throw(Form::NotSignedException, Form::GradeTooLowException);

	protected:
		ShrubberyCreationForm();
};

#endif