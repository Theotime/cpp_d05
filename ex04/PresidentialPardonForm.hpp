#ifndef __PRESIDENTIALPARDONFORM_HPP__
# define __PRESIDENTIALPARDONFORM_HPP__

#include "Form.hpp"

class PresidentialPardonForm : public Form {

	public:
		PresidentialPardonForm(std::string const &target);
		PresidentialPardonForm(PresidentialPardonForm const &obj);
		~PresidentialPardonForm();

		PresidentialPardonForm				&operator=(PresidentialPardonForm const &obj);

		virtual void						execute(Bureaucrat const &obj) const throw(Form::NotSignedException, Form::GradeTooLowException);

	protected:
		PresidentialPardonForm();

};

#endif