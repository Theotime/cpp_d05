#ifndef __INTERN_HPP__
# define __INTERN_HPP__

# include "Form.hpp"

class Intern {

	public:
		Intern();
		Intern(Intern const &obj);
		~Intern();

		Intern				&operator=(Intern const &obj);

		Form				*makeForm(std::string const &type, std::string const &target) const;

};

#endif