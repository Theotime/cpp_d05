#include "ShrubberyCreationForm.hpp"
#include <fstream>

ShrubberyCreationForm::ShrubberyCreationForm() {}
ShrubberyCreationForm::ShrubberyCreationForm(std::string const &target) : Form("ShrubberyCreationForm", target, 25, 5) {}
ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &obj) {
	*this = obj;
}
ShrubberyCreationForm::~ShrubberyCreationForm() {}

ShrubberyCreationForm				&ShrubberyCreationForm::operator=(ShrubberyCreationForm const &obj) {
	Form::operator=(obj);
	return (*this);
}

void							ShrubberyCreationForm::execute(Bureaucrat const &obj) const throw(Form::NotSignedException, Form::GradeTooLowException) {
	Form::execute(obj);
	std::ofstream file((this->getTarget() + "_shrubbery").c_str());
	if (file.is_open()) {
		file << "ASCII trees";
		file.close();
	}
}