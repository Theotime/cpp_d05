#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm() {}
RobotomyRequestForm::RobotomyRequestForm(std::string const &target) : Form("RobotomyRequestForm", target, 72, 45) {}
RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &obj) {
	*this = obj;
}
RobotomyRequestForm::~RobotomyRequestForm() {}

RobotomyRequestForm				&RobotomyRequestForm::operator=(RobotomyRequestForm const &obj) {
	Form::operator=(obj);
	return (*this);
}

void							RobotomyRequestForm::execute(Bureaucrat const &obj) const throw(Form::NotSignedException, Form::GradeTooLowException) {
	static bool			work = true;

	Form::execute(obj);
	if (work)
		std::cout << "that " << obj.getName() << " has been robotomized successfully" << std::endl;
	else
		std::cout << "that " << obj.getName() << " hasn't been robotomized successfully" << std::endl;
	work = !work;
}