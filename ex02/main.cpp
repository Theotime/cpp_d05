#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"

int				main() {
	{
		std::cout << std::endl << "STUDENT GRADE 42" << std::endl;
		Bureaucrat					student("Student", 42);
		PresidentialPardonForm		f1("Kwame");
		RobotomyRequestForm			f2("NS");
		ShrubberyCreationForm		f3("KRP");
		
		student.signForm(f1);
		student.executeForm(f2);
		student.signForm(f3);
	}
	{
		std::cout << std::endl << "STUDENT GRADE 150" << std::endl;
		Bureaucrat					student("Student", 150);
		PresidentialPardonForm		f1("Kwame");
		RobotomyRequestForm			f2("NS");
		ShrubberyCreationForm		f3("KRP");
		
		student.signForm(f1);
		student.executeForm(f2);
		student.signForm(f3);
	}
	{
		std::cout << std::endl << "STUDENT GRADE 20" << std::endl;
		Bureaucrat					student("Student", 20);
		PresidentialPardonForm		f1("Kwame");
		RobotomyRequestForm			f2("NS");
		ShrubberyCreationForm		f3("KRP");
		
		student.signForm(f1);
		student.executeForm(f2);
		student.signForm(f3);
	}
	{
		std::cout << std::endl << "STUDENT GRADE 20" << std::endl;
		Bureaucrat					student("Student", 1);
		PresidentialPardonForm		f1("Kwame");
		RobotomyRequestForm			f2("NS");
		ShrubberyCreationForm		f3("KRP");
		
		student.signForm(f1);
		student.executeForm(f1);
		student.signForm(f2);
		student.executeForm(f2);
		student.signForm(f3);
		student.executeForm(f3);
	}
}