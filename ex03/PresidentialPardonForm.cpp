#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm() {}
PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const &obj) {
	*this = obj;
}

PresidentialPardonForm::PresidentialPardonForm(std::string const &target) : Form("PresidentialPardonForm", target, 145, 137) {}

PresidentialPardonForm::~PresidentialPardonForm() {}

PresidentialPardonForm			&PresidentialPardonForm::operator=(PresidentialPardonForm const &obj) {
	(void)obj;
	return (*this);
}

void							PresidentialPardonForm::execute(Bureaucrat const &obj) const throw(Form::NotSignedException, Form::GradeTooLowException)  {
	Form::execute(obj);
	std::cout << obj.getName() << " has been pardoned by Zafod Beeblebrox." << std::endl;
}