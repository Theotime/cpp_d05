#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "Intern.hpp"

int				main() {
	Intern			toto;
	Form			*f;

	f = toto.makeForm("presidential pardon", "NS");
	std::cout << *f << std::endl;
	delete f;

	f = toto.makeForm("robotomy request", "KRP");
	std::cout << *f << std::endl;
	delete f;

	f = toto.makeForm("shrubbery creation", "Kwame");
	std::cout << *f << std::endl;
	delete f;

	f = toto.makeForm("FORM NOT EXISTS", "FAIL");
	if (!f)
		std::cout << "FORM NOT EXISTS AND CANNOT BE CREATED" << std::endl;

	return (0);
}