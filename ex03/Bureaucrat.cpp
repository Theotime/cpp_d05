#include "Bureaucrat.hpp"
#include "Form.hpp"

Bureaucrat::Bureaucrat() {}

Bureaucrat::Bureaucrat(std::string const &name, int grade) : _name(name) {
	if (grade < 1)
		throw Bureaucrat::GradeTooHighException();
	else if (grade > 150)
		throw Bureaucrat::GradeTooLowException();
	this->_grade = grade;
}

Bureaucrat::Bureaucrat(Bureaucrat const &obj) {
	*this = obj;
}
Bureaucrat::~Bureaucrat() {}

void					Bureaucrat::signForm(Form &form) {
	try {
		form.beSigned(*this);
		std::cout << this->getName() << " signs " << form.getName() << std::endl;
	} catch (Form::GradeTooLowException e) {
		std::cout << this->getName() << " cannot sign " << form.getName() << " because " << e.what() << std::endl;
	}
}

void					Bureaucrat::executeForm(Form &form) {
	try {
		form.execute(*this);
		std::cout << this->getName() << " executes " << form.getName() << std::endl;
	} catch (Form::GradeTooLowException e) {
		std::cout << this->getName() << " cannot be executes " << form.getName() << " because : " << e.what() << std::endl;
	}  catch (Form::NotSignedException e) {
		std::cout << this->getName() << " cannot be executes " << form.getName() << " because : " << e.what() << std::endl;
	}
}

Bureaucrat				&Bureaucrat::operator=(Bureaucrat const &obj) {
	this->_grade = obj._grade;
	this->_name = obj._name;
	return (*this);
}

std::ostream			&operator<<(std::ostream &stream, Bureaucrat const &obj) {
	stream << obj.getName() << ", bureaucrat grade " << obj.getGrade();
	return (stream);
}

void					Bureaucrat::up(void) {
	if (this->_grade == 1)
		throw Bureaucrat::GradeTooHighException();
	this->_grade--;
}

void					Bureaucrat::down(void) {
	if (this->_grade == 150)
		throw Bureaucrat::GradeTooLowException();
	this->_grade++;
}

int						Bureaucrat::getGrade(void) const {
	return (this->_grade);
}

std::string	const		Bureaucrat::getName(void) const {
	return (this->_name);
}