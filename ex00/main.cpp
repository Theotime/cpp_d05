#include "Bureaucrat.hpp"

int				main() {
	Bureaucrat		hollande("Hollande", 145);
	Bureaucrat		sarkozy("Sarkozy", 5);
	Bureaucrat		*toto;

	std::cout << "TRY TO DOWN GRADE" << std::endl;
	try {
		for (size_t i = 0; i < 150; ++i) {
			hollande.down();
			std::cout << "	" << hollande << std::endl;
		}
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "TRY TO UP GRADE" << std::endl;
	try {
		for (size_t i = 0; i < 150; ++i) {
			sarkozy.up();
			std::cout << "	" << sarkozy << std::endl;
		}
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "TRY TO INIT WITH HIGHTER GRADE" << std::endl;
	try {
		toto = new Bureaucrat("Toto", 0);
		delete toto;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "TRY TO INIT WITH LOWER GRADE" << std::endl;
	try {
		toto = new Bureaucrat("Toto", 151);
		delete toto;
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}

}