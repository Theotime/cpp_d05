#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat() {}

Bureaucrat::Bureaucrat(std::string const &name, int grade) : _name(name) {
	if (grade < 1)
		throw Bureaucrat::GradeTooHighException();
	else if (grade > 150)
		throw Bureaucrat::GradeTooLowException();
	this->_grade = grade;
}

Bureaucrat::Bureaucrat(Bureaucrat const &obj) {
	*this = obj;
}
Bureaucrat::~Bureaucrat() {}

Bureaucrat				&Bureaucrat::operator=(Bureaucrat const &obj) {
	this->_grade = obj._grade;
	this->_name = obj._name;
	return (*this);
}

std::ostream			&operator<<(std::ostream &stream, Bureaucrat const &obj) {
	stream << obj.getName() << ", bureaucrat grade " << obj.getGrade();
	return (stream);
}

void					Bureaucrat::up(void) {
	if (this->_grade == 1)
		throw Bureaucrat::GradeTooHighException();
	this->_grade--;
}

void					Bureaucrat::down(void) {
	if (this->_grade == 150)
		throw Bureaucrat::GradeTooLowException();
	this->_grade++;
}

int						Bureaucrat::getGrade(void) const {
	return (this->_grade);
}

std::string	const		Bureaucrat::getName(void) const {
	return (this->_name);
}