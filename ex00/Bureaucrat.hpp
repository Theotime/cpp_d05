#ifndef __BUREAUCRAT_HPP__
# define __BUREAUCRAT_HPP__

# include <iostream>
# include <stdexcept>

class Bureaucrat {

	public:
		Bureaucrat(std::string const &name, int grade);
		Bureaucrat(Bureaucrat const &obj);
		~Bureaucrat();

		Bureaucrat			&operator=(Bureaucrat const &obj);

		void				up(void);
		void				down(void);

		int					getGrade(void) const;
		std::string const	getName() const;

		class GradeTooHighException : public std::exception {
			public:
				virtual const char*			what() const throw() {
					return ("EXCEPTION -> Bureaucrat::GradeTooHighException");
				}
		};

		class GradeTooLowException : public std::exception {
			public:
				virtual const char*			what() const throw() {
					return ("EXCEPTION -> Bureaucrat::GradeTooLowException");
				}
		};

	private:
		Bureaucrat();

		std::string		_name;
		int				_grade;

};

std::ostream			&operator<<(std::ostream &stream, Bureaucrat const &obj);

#endif